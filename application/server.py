import logging
from logging.config import dictConfig

from application.config import config
from application.proxy import get_server_instance

dictConfig(
    {
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s %(funcName)s (line %(lineno)s): %(message)s",
                "datefmt": "%d.%m.%Y %H:%M:%S",
            }
        },
        "handlers": {"stdout": {"level": "INFO", "class": "logging.StreamHandler", "formatter": "default"}},
        "loggers": {"": {"handlers": ["stdout"], "propagate": True, "level": config.log_level}},
    }
)

logger = logging.getLogger(__name__)


if __name__ == "__main__":
    logger.info(f"Starting server at {config.netloc}")

    try:
        server = get_server_instance()
        server.serve_forever()
    except KeyboardInterrupt:
        ...
    except Exception as e:
        logger.exception("Error at started server")

        raise e
