import logging
import re
from http.server import BaseHTTPRequestHandler, HTTPServer
from io import StringIO
from urllib.parse import urlparse

import requests
from lxml import etree
from lxml.etree import _Element

from application.config import config

__all__ = ["get_server_instance"]

logger = logging.getLogger(__name__)


class ProxyHTTPRequestHandler(BaseHTTPRequestHandler):
    _POSTFIX = "™"
    _PROXY_HOSTS = ["habr.com"]
    _TAGS = ["html", "body", "section", "div", "span", "p", "b"]

    def do_GET(self):
        response = self._make_request(self.path)

        self.send_response(response.status_code)
        self.end_headers()

        is_target = self._check_target_url(self.path)
        content_type = response.headers.get("content-type", "")
        if is_target and response.ok and "text/html" in content_type:
            content = self._modify_content(response.text)
        else:
            content = response.text.encode("utf-8")
        self.wfile.write(content)

    def _make_request(self, url: str) -> requests.Response:
        try:
            return requests.get(url, verify=False)
        except Exception as e:
            logger.exception(f"GET {url} was failure")

    def _check_target_url(self, path: str) -> bool:
        url = urlparse(path)

        return url.hostname in self._PROXY_HOSTS

    @classmethod
    def _modify_content(cls, text: str) -> bytes:
        try:
            parser = etree.HTMLParser()
            tree = etree.parse(StringIO(text), parser)

            cls._process_element(tree.getroot())
            changed_text = etree.tostring(tree.getroot(), pretty_print=True, method="html")
        except Exception as e:
            changed_text = b"Error at changing content..."
            logger.exception("Changing content was failure")

        return changed_text

    @classmethod
    def _process_element(cls, element: _Element):
        for child in element.getchildren():
            if child.tag in cls._TAGS and child.text:
                words = []
                for word in re.compile("([\w][\w]*'?\w?)").findall(child.text):
                    if len(word) == 6:
                        word += cls._POSTFIX
                    words.append(word)
                child.text = " ".join(words).strip()

            cls._process_element(child)


def get_server_instance():
    return HTTPServer((config.host, config.port), ProxyHTTPRequestHandler)
