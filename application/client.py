import sys

import requests

from application.config import config

if __name__ == "__main__":
    response = requests.get(sys.argv[1], proxies={"http": config.netloc, "https": config.netloc}, verify=False)
    print(response.text)
