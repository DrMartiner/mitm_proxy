from pydantic import BaseSettings

__all__ = ["config"]


class Config(BaseSettings):
    host: str = "127.0.0.1"
    port: int = 8080

    log_level: str = "INFO"

    class Config:
        env_prefix = "PROXY_"

    @property
    def netloc(self) -> str:
        return f"{self.host}:{self.port}"


config = Config()
