#!/usr/bin/env bash

COMMAND=$1

case "${1}" in
    "bash")
        shift
        exec bash -c "${args[@]:1}"
        ;;
    *)
        shift
        exec make ${COMMAND}
        ;;
esac
