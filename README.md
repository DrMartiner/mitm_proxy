[![pipeline status](https://gitlab.com/DrMartiner/mitm_proxy/badges/master/pipeline.svg)](https://gitlab.com/DrMartiner/mitm_proxy/commits/master)

# How to use

This is simple HTTP(s) proxy

## At local
### Deploy
```bash
pyenv virtualenv 3.7.4 mitm_proxy
pyenv activate mitm_proxy

pip install poetry black

poetry install
```

### Configure (default values)
```bash
export PROXY_HOST=127.0.0.1
export PROXY_PORT=8080
export PROXY_LOG_LEVEL=INFO
```

### Run server
```bash
make server
```

### Call test client
```bash
make client url=http://habr.com/ru/ > page.html
open page.html
```

### Testing
Call ```make isort black``` before commit changes.

```bash
make black tests
```

# Original task
Реализовать простой http-прокси-сервер, запускаемый локально (порт на ваше усмотрение), который показывает содержимое страниц Хабра. Прокси должен модифицировать текст на страницах следующим образом: после каждого слова из шести букв должен стоять значок «™». Пример: 

Исходный текст: [https://habr.com/ru/company/yandex/blog/258673/](https://habr.com/ru/company/yandex/blog/258673/)

Сейчас на фоне уязвимости Logjam все в индустрии в очередной раз обсуждают проблемы и особенности TLS. Я хочу воспользоваться  этой возможностью, чтобы поговорить об одной из них, а именно — о настройке ciphersiutes.
Через  ваш прокси http://127.0.0.1:8232/company/yandex/blog/258673/
Сейчас™ на фоне уязвимости Logjam™ все в индустрии в очередной раз обсуждают проблемы и особенности TLS. Я хочу воспользоваться этой возможностью, чтобы поговорить об одной из них, а именно™ — о настройке ciphersiutes. 
Условия:
- Python™ 3.5+
- страницы должны™ отображаться и работать полностью корректно, в точности так, как и оригинальные (за исключением модифицированного текста™);
при навигации по ссылкам, которые ведут на другие™ страницы хабра, браузер должен™ оставаться на адресе™ вашего™ прокси™;
можно использовать любые общедоступные библиотеки, которые сочтете нужным™;
чем меньше™ кода, тем лучше. PEP8 — обязательно;
- если в условиях вам не хватает каких-то данных™, опирайтесь на здравый смысл.

Если задачу™ удалось сделать быстро™, и у вас еще остался энтузиазм - как насчет™ написания тестов™?
Присылайте ваше решение в виде ссылки на gist или на публичный репозиторий на Github.
