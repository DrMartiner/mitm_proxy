isort:
	@echo -n "Run isort"
	isort -rc application tests

.black: $(shell find application -type d)
	black application tests
	@if ! isort -c -rc application tests; then \
		echo "Import sort errors. Run 'make isort' to fix it"; \
		isort --diff -rc application; \
		false; \
	fi

.PHONY: black
black: .black
	@echo "Run black and isort"

.PHONY: tests
tests:
	PYTHONPATH=. pytest -svvv -rs --cov=application --cov-report=html -x tests

server:
	python -m application.server

client:
	python -m application.client $(url)

.PHONY: clean
clean:
	@echo -n "Clear tmp files"
	@rm -rf `find . -type f -name '#*#' `
	@rm -rf `find . -type f -name '@*' `
	@rm -rf `find . -type f -name '*~' `
	@rm -rf `find . -type f -name '*.py[co]' `
	@rm -rf `find . -type f -name '.*~' `
	@rm -rf `find . -name __pycache__`
	@rm -rf .coverage
	@rm -rf coverage.html
	@rm -rf coverage.xml
	@rm -rf htmlcov
	@rm -rf cover
	@rm -rf .develop
	@rm -rf build
	@rm -rf *.egg-info
	@rm -rf .flake
	@rm -rf .install-deps
	@rm -rf dist
	@rm -rf .pytest_cache
