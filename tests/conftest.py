from http.server import HTTPServer
from threading import Thread
from typing import Callable

import pytest
import requests
from requests import Response

from application.proxy import get_server_instance


class ThreadServer(Thread):
    daemon = True

    _httpd: HTTPServer
    _host = "127.0.0.1"
    _port = 8080

    def __init__(self, *args, **kwargs):
        self._server = get_server_instance()

        super(ThreadServer, self).__init__(target=self._server.serve_forever, *args, **kwargs)

    def stop(self):
        self._server.server_close()

    @property
    def server(self) -> HTTPServer:
        return self._server


@pytest.yield_fixture(scope="session")
def server() -> HTTPServer:
    thread = ThreadServer()
    thread.start()

    yield thread.server

    thread.stop()


@pytest.fixture
def get_response() -> Callable:
    def _get_request(url: str) -> Response:
        return requests.get(url, proxies={"http": "127.0.0.1:8080"}, verify=False)

    return _get_request
