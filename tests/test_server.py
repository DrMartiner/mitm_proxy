import mock
import pytest
from requests import Response

from application.proxy import ProxyHTTPRequestHandler as Handler


@pytest.mark.parametrize("status_code", [200, 400, 500])
def test_response(status_code, server, get_response):
    url = "http://habr.com/path"

    with mock.patch("application.proxy.ProxyHTTPRequestHandler._make_request") as patch:  # type: mock.MagicMock
        fake_response = Response()
        fake_response._content = b"content"
        fake_response.status_code = status_code

        patch.return_value = fake_response

        response: Response = get_response(url)

        assert patch.called
        assert patch.call_count == 1
        assert patch.call_args[0][0] == url

    assert response.status_code == status_code
    assert response.text == fake_response.text


def test_modify_content():
    html = """
    <html>
        <script>123456</script>
        <section>
        <div><span>123456</span></div>
        <div>
            <span>12345</span>
            <p>
                <custom-tag>123456</custom-tag>
            </p>
        </div>
        </section>
    </html>"""
    modified_content = Handler._modify_content(html)
    modified_content = modified_content.decode("utf-8")

    assert "<span>12345</span>" in modified_content
    assert "<custom-tag>123456</custom-tag>" in modified_content
    assert f"<span>123456&#8482;</span>" in modified_content
