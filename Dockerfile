FROM python:3.7.4

ENV PYTHONDONTWRITEBYTECODE 1
ENV WORK_DIR "/application/src"
ENV USER user
ENV GROUP usergroup
ENV PROXY_PORT 8080

RUN mkdir -p ${WORK_DIR}
WORKDIR ${WORK_DIR}

RUN pip install poetry &&\
    poetry config virtualenvs.create false

ADD poetry.lock ${WORK_DIR}
ADD pyproject.toml ${WORK_DIR}
RUN poetry install --no-dev

ADD . ${WORK_DIR}

RUN addgroup --system ${GROUP} &&\
    adduser --system --home /application/user --ingroup ${GROUP} ${USER} --shell /bin/bash &&\
    chown -R ${USER}:${GROUP} /application
USER ${USER}

EXPOSE ${PROXY_PORT}

ENTRYPOINT ["/application/src/docker-entrypoint.sh"]
CMD ["server"]
